'use strict';
var Foxx = require('org/arangodb/foxx');
var joi = require('joi');

module.exports = Foxx.Model.extend({
  schema: {
    // Describe the attributes with joi here
    _key: joi.string(),
      realName:joi.string(),
      activities:joi.array(),
      users:joi.array()
  }
});