var fs = require('fs');
var http = require('http');
var _ = require('lodash');
var queryString = require('querystring');
var finalActivitesArray = [];

fs.readFile('seedData/seedActivities.txt', function (err, data) {
    if (err) throw err;
    var activitiesArray = data.toString().split("\n");
    var uniqueactivitiesArray = _.unique(activitiesArray);

    for (i in uniqueactivitiesArray) {
        // creating a unique name for each activity to reduce the duplication
        var activityUniqueName = activitiesArray[i].trim().replace(/\s+/g, '').toLowerCase().replace(/[`~!#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
        var randomNumber = Math.floor((Math.random() * 1000) + 1);
        var activity = {
            "_key": activityUniqueName,
            "realName": activitiesArray[i].trim(),
            "likes": randomNumber
        };
        finalActivitesArray.push(activity);
    }
    // write data to json file
    fs.writeFile('seedData/seedActivities.json',JSON.stringify(finalActivitesArray), function (err) {
        if (err) return console.log(err);
        console.log('Activities are stored now in seedActivities.json');
    });
    // inserting activities in our database using node http request
    for (var i = 0; i < finalActivitesArray.length; i++) {
        var data = JSON.stringify(finalActivitesArray[i]);
       // var data = finalActivitesArray[i];
        var options = {
            host: 'localhost',
            port: 8529,
            path: '/_db/_system/wishpool/activities',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        };
        var req = http.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (data) {
                console.log("body: " + data);
            });
        });
        req.write(data);
        req.end();
    };
});
