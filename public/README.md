wishpool - your wishes come to real life
=====================================

This is the io.js server part of the app. The whole code including
the ArangoDB Foxx app can be found at
[bitbucket-repository](https://bitbucket.org/AnasFullStack/wishpool).

Installation
------------

Install [ArangoDB](http://www.arangodb.com) and in there the Foxx app
with the above github URL. Then do

    npm install wishpool
    npm start wishpool

preferably in a screen session and visit port 8000 on the machine you
are running everything on.
