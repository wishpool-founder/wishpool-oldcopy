angular.module('wishpool', ['ui.router','ngSanitize', 'MassAutoComplete'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/views/login.html',
                controller: 'loginController'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: '/views/profile.html',
                controller: 'profileController'
            })
            .state('activities', {
                url: '/activities',
                templateUrl: '/views/activities.html',
                controller: 'activitiesController'
            })
            .state('addNewGroup', {
                url: '/addNewGroup',
                templateUrl: '/views/addNewGroup.html',
                controller: 'addNewGroupController'
            })
            .state('allGroups', {
                url: '/allGroups',
                templateUrl: '/views/allGroups.html',
                controller: 'allGroupsController'
            })
    });