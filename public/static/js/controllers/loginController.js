angular.module('wishpool').controller('loginController', ['$scope', '$http', 'sharedService', '$window',
    function ($scope, $http, sharedService, $window) {
        // redirect to profile if user is logged in
        var logged = sharedService.checkLogin();
        if (logged) {
            $window.location.href = '#/profile'
        }
        //initializing user object
        $scope.user = {
            name: "",
            pass: ""
        };
        // sign in temporary function
        $scope.signIn = function () {
            if ($scope.user.name!=""){
                // check the existence of this user
                $http.get('http://localhost:8529/_db/_system/wishpool/users/' + $scope.user.name).
                    success(function (data, status, headers, config) {
                        // if this is a current user create a session with user name and redirect to it's profile
                        console.log(data);
                        sharedService.registerLoggedUser(data);
                        $window.location.href = '#/profile'
                    }).
                    error(function (data, status, headers, config) {
                        // if it is a new user add it to the database then redirect it to it's profile
                        console.log(data);
                        var newUser = {
                            "_key": $scope.user.name
                        };
                        $http.post('http://localhost:8529/_db/_system/wishpool/users', newUser).
                            success(function (data, status, headers, config) {
                                console.log(data);
                                sharedService.registerLoggedUser(data);
                                $window.location.href = '#/profile'
                            }).
                            error(function (data, status, headers, config) {
                                console.log(data)
                            })
                    });
            }else{
                alert('You have to fill user name first');
            }

        }
    }]);