angular.module('wishpool').controller('activitiesController', ['$scope', '$http', 'sharedService', '$filter',
    function ($scope, $http, sharedService, $filter) {
        $scope.activityFilter = "";
        $scope.showLikedActivityAlert = false;
        // Get all activities and their likes
        var getAllActivities = function () {
            $http.get('http://localhost:8529/_db/_system/wishpool/activities').
                success(function (data) {
                    $scope.activities = data;
                });
        };
        getAllActivities();

        $scope.likeActivity = function (activity) {
            // increasing likes of the clicked activity
            var activityPatchData = {
                "likes": activity.likes + 1
            };
            $scope.showLikedActivityAlert = false;
            // checking if the logged in user liked this activity before, if he did not add it to his activities and increase activity likes
            sharedService.getCurrentUserData().then(function (data) {
                var userActivities = data.data.activities || [];
                var isLikedActivityInUserData = $filter('filter')(userActivities, {_key: activity._key});
                console.log(isLikedActivityInUserData)
                if (isLikedActivityInUserData == false || isLikedActivityInUserData == undefined) {
                    var addedActivity = {
                        "_key": activity._key,
                        "realName": activity.realName
                    };
                    userActivities.push(addedActivity);
                    var patchedActivities = {"activities": userActivities};
                    $http.patch('http://127.0.0.1:8529/_db/_system/wishpool/users/' + data.data._key, patchedActivities).
                        success(function (data) {
                            $http.patch('http://127.0.0.1:8529/_db/_system/wishpool/activities/' + activity._key, activityPatchData).
                                success(function (data) {
                                    getAllActivities();
                                });
                        });
                } else {
                    $scope.showLikedActivityAlert = true;
                }

            })

        }

    }]);